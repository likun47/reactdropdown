import React from 'react';
import './AutoCompleteText.css';

export default class AutoComleteText extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            suggestions: [],
            text: '',
            trigger: false
        }
    };

    getLabels = (keyword) => {
        const allLabels = ['Someday_Action','NextActions','waiting','Alexa','Investigation','pendingDesignDiscussion','costco'];
        const result = allLabels.filter(function (x) {
            return x.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
        });
        return result;
    };
    getLabelsAsync = (keyword) => {
        const result = this.getLabels(keyword);
        const delay = Math.random() * 800 + 200; // delay 200~1000ms
        return new Promise(function (resolve, reject) {
            setTimeout(resolve, delay, result); // result will send out by resolve
        });
    };

    onTextChanged = (e) => {
        const value = e.target.value;
        if (value.includes("@")) {
            this.setState({
                trigger: true,
                suggestions: ['Someday_Action','NextActions','waiting','Alexa','Investigation','pendingDesignDiscussion']
            })
        } else {
            this.setState({
                trigger: false,
            })
        }

        if (value.slice(value.indexOf("@")+1).length > 0) {
            this.getLabelsAsync(value.slice(value.indexOf("@")+1)).then(array => {
                console.log(array);
                this.setState({
                    suggestions: array
                })
            });
        }
        this.setState(() => ({ text: value }));
    };
    
    
    suggestionSelected(value) {
        this.setState(() => ({
            text: value,
        }))
    }
    renderSuggestion() {
        const { suggestions } = this.state;
        if (suggestions.length === 0) {
            return null;
        }
        if (this.state.trigger) {
            return (
                <ul>
                    {suggestions.map((item, i) => <li key={i} onClick={() => this.suggestionSelected(item)}>{item}</li>)}
                </ul>
            )
        }
    }
    render() {
        const { text } = this.state;
        return (
            <div className="AutoCompleteText">
                <input value={text} type="text" onChange={this.onTextChanged} />
                {this.renderSuggestion()}
            </div>
        )
    }
}